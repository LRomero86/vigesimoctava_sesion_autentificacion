const express = require('express');
const helmet = require('helmet');
const jwt = require('jsonwebtoken');

const informacion = {
    nombre: "Maria la del barrio",
    id:1,
}

const firma = "esta_es_la_clave_secreta";
    
//firmar
const token = jwt.sign(informacion, firma);
console.log(token);

//descodificar
const descodificar = jwt.decode(token, firma);
console.log(descodificar);

const app = express();


app.use(express.json());

//agregar seguridad a nuestro servidor usando helmet
app.use(helmet());

app.post('/login', (req, res) => {
    const {usuario, contrasenia} = req.body;
    const valido = validarUsuario(usuario, contrasenia);

    if(!valido) {
        res.json({error: 'Usuario con contraseña incorrecta'});
        return;
    }

    const token = jwt.sign({usuario}, firma);
});


const autenticarUsuario = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        console.log('token desde postman header', token);
        const verificarToken = jwt.verify(token, firma);

        if(verificarToken) {
            req.usuario = verificarToken;
            return next();
        } else {
            res.send('Error de login');
        }
    } catch (error) {
        res.json({error_m: "Error al validar"});
    }
};

const validarUsuario = (usuario, contrasenia) => {
    try {
        const claves = [
            {
                usuario:'Luciano',
                contrasenia:'1234'
            },
            {
                usuario:'Maria',
                contrasenia:'135'
            },
        ];

        const valido = claves.filter(x => x.contrasenia === contrasenia && x.usuario === usuario);
        if(valido.length > 0) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        res.send(error);
    }
};

app.post('/seguro', autenticarUsuario, (req, res) => {
    res.send(`esta es una página autenticada ${req.usuario.usuario}`);
});


app.listen(3000, () => {
    console.log('Servidor ejecutándose en puerto 3000');
});