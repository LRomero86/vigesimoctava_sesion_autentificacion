const express = require('express');
const helmet = require('helmet');
const jwt = require('jsonwebtoken');

const app = express();

const informacion = {
    id: 1,
    nombre: 'Pepe',
    pass: 'lalala',
}

const token = jwt.sign(informacion.nombre, informacion.pass);
console.log(token);
    
//verify
const decode = jwt.verify(token, informacion.pass);

app.listen(3000, () => {
    console.log('Servidor ejecutándose en puerto 3000');
});